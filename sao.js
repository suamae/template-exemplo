module.exports = {
	prompts: {
		nome: { 
			type: 'string',
			message: "Nome do projeto:"
		},
		browser_ie: { 
			type: 'list',
			message: "Selecione a versão mínima do IE para suportar",
			default: "ie9",
			choices: [
				"ie7",
				"ie8",
				"ie9",
				"ie10",
				"ie11",
				"ie12",
				"edge",
			]
		},
		browser_firefox: { 
			type: 'list',
			message: "Selecione a versão mínima do Firefox",
			choices: [
				"33",
				"Versões Recentes"
			]
		},
	},
};